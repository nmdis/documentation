# translation of Inkscape tutorial interpolate to Slovak
# Copyright (C)
# This file is distributed under the same license as the Inkscape package.
# Ivan Masár <helix84@centrum.sk>, 2010, 2015.
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Interpolate\n"
"POT-Creation-Date: 2018-02-15 21:47+0100\n"
"PO-Revision-Date: 2015-01-30 15:44+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#: interpolate-f20.svg:55(format) interpolate-f19.svg:52(format)
#: interpolate-f18.svg:45(format) interpolate-f17.svg:45(format)
#: interpolate-f16.svg:45(format) interpolate-f15.svg:45(format)
#: interpolate-f14.svg:45(format) interpolate-f13.svg:45(format)
#: interpolate-f12.svg:45(format) interpolate-f11.svg:52(format)
#: interpolate-f10.svg:45(format) interpolate-f09.svg:45(format)
#: interpolate-f08.svg:45(format) interpolate-f07.svg:45(format)
#: interpolate-f05.svg:45(format) interpolate-f04.svg:47(format)
#: interpolate-f03.svg:45(format) interpolate-f02.svg:47(format)
#: interpolate-f01.svg:45(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: interpolate-f11.svg:113(flowPara) interpolate-f04.svg:108(flowPara)
#: interpolate-f02.svg:117(flowPara)
#, no-wrap
msgid "Exponent: 0.0"
msgstr "Exponent: 0.0"

#: interpolate-f11.svg:114(flowPara) interpolate-f04.svg:109(flowPara)
#: interpolate-f02.svg:118(flowPara)
#, no-wrap
msgid "Interpolation Steps: 6"
msgstr "Krokov interpolácie: 6"

#: interpolate-f11.svg:115(flowPara) interpolate-f04.svg:110(flowPara)
#: interpolate-f02.svg:119(flowPara)
#, no-wrap
msgid "Interpolation Method: 2"
msgstr "Metóda interpolácie: 2"

#: interpolate-f11.svg:116(flowPara) interpolate-f04.svg:111(flowPara)
#: interpolate-f02.svg:120(flowPara)
#, no-wrap
msgid "Duplicate Endpaths: unchecked"
msgstr "Duplikovať konce ciest: nezaškrtnuté"

#: interpolate-f11.svg:117(flowPara) interpolate-f04.svg:112(flowPara)
#: interpolate-f02.svg:121(flowPara)
#, no-wrap
msgid "Interpolate Style: unchecked"
msgstr "Interpolovať štýl: nezaškrtnuté"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:27(None)
msgid "@@image: 'interpolate-f01.svg'; md5=97881f947107640f7b8e333a949ff14c"
msgstr "@@image: 'interpolate-f01.svg'; md5=97881f947107640f7b8e333a949ff14c"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:36(None)
msgid "@@image: 'interpolate-f02.svg'; md5=d980264e812797c07f9190923bb211b5"
msgstr "@@image: 'interpolate-f02.svg'; md5=d980264e812797c07f9190923bb211b5"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:51(None)
msgid "@@image: 'interpolate-f03.svg'; md5=28c072ceeacdb2bdd7c2259ffbe599ad"
msgstr "@@image: 'interpolate-f03.svg'; md5=28c072ceeacdb2bdd7c2259ffbe599ad"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:60(None) tutorial-interpolate.xml:82(None)
msgid "@@image: 'interpolate-f04.svg'; md5=73d700c3c612f13f30dff2c93f935351"
msgstr "@@image: 'interpolate-f04.svg'; md5=73d700c3c612f13f30dff2c93f935351"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:73(None)
msgid "@@image: 'interpolate-f05.svg'; md5=4b4454eb3eb522658e217d20187e32e4"
msgstr "@@image: 'interpolate-f05.svg'; md5=4b4454eb3eb522658e217d20187e32e4"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:91(None)
msgid "@@image: 'interpolate-f07.svg'; md5=0c542eea7053a06ef83d5cc38b6cfd95"
msgstr "@@image: 'interpolate-f07.svg'; md5=0c542eea7053a06ef83d5cc38b6cfd95"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:98(None)
msgid "@@image: 'interpolate-f08.svg'; md5=e6cecf42fbd1025be356f25776f3ec8b"
msgstr "@@image: 'interpolate-f08.svg'; md5=e6cecf42fbd1025be356f25776f3ec8b"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:113(None)
msgid "@@image: 'interpolate-f09.svg'; md5=2c83c895aa047a9420a97089045065b4"
msgstr "@@image: 'interpolate-f09.svg'; md5=2c83c895aa047a9420a97089045065b4"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:122(None)
msgid "@@image: 'interpolate-f10.svg'; md5=4c6045fda8b91263faa9d0aa545a1786"
msgstr "@@image: 'interpolate-f10.svg'; md5=4c6045fda8b91263faa9d0aa545a1786"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:140(None)
msgid "@@image: 'interpolate-f11.svg'; md5=b1955b330bb469376a0f53627d6fb12e"
msgstr "@@image: 'interpolate-f11.svg'; md5=b1955b330bb469376a0f53627d6fb12e"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:149(None)
msgid "@@image: 'interpolate-f12.svg'; md5=f54c78050ee73b5ac8438dd9dfb26a58"
msgstr "@@image: 'interpolate-f12.svg'; md5=f54c78050ee73b5ac8438dd9dfb26a58"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:158(None)
msgid "@@image: 'interpolate-f13.svg'; md5=0e4c6243f3cecfe5c0c3d640c1df8433"
msgstr "@@image: 'interpolate-f13.svg'; md5=0e4c6243f3cecfe5c0c3d640c1df8433"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:167(None)
msgid "@@image: 'interpolate-f14.svg'; md5=85bacb3a295c8a8b74207f2433967ef6"
msgstr "@@image: 'interpolate-f14.svg'; md5=85bacb3a295c8a8b74207f2433967ef6"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:178(None)
msgid "@@image: 'interpolate-f15.svg'; md5=f24c95652fb988c9c2d71e6028ee1ffe"
msgstr "@@image: 'interpolate-f15.svg'; md5=f24c95652fb988c9c2d71e6028ee1ffe"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:197(None)
msgid "@@image: 'interpolate-f16.svg'; md5=b209d212b7e65ba40f49182ac54eab66"
msgstr "@@image: 'interpolate-f16.svg'; md5=b209d212b7e65ba40f49182ac54eab66"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:207(None)
msgid "@@image: 'interpolate-f17.svg'; md5=ed2c2700c8175dd6529e6cc29d43e7f8"
msgstr "@@image: 'interpolate-f17.svg'; md5=ed2c2700c8175dd6529e6cc29d43e7f8"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:216(None)
msgid "@@image: 'interpolate-f18.svg'; md5=0d4ffd793a48f69837394264b130f012"
msgstr "@@image: 'interpolate-f18.svg'; md5=0d4ffd793a48f69837394264b130f012"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:228(None)
msgid "@@image: 'interpolate-f19.svg'; md5=0645e0f056b2922d4f33909d9cdb12ac"
msgstr "@@image: 'interpolate-f19.svg'; md5=0645e0f056b2922d4f33909d9cdb12ac"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-interpolate.xml:237(None)
msgid "@@image: 'interpolate-f20.svg'; md5=0011eff8a109c5caf114ac98740ccd2d"
msgstr "@@image: 'interpolate-f20.svg'; md5=0011eff8a109c5caf114ac98740ccd2d"

#: tutorial-interpolate.xml:4(title)
msgid "Interpolate"
msgstr "Interpolácia"

#: tutorial-interpolate.xml:5(author)
msgid "Ryan Lerch, ryanlerch at gmail dot com"
msgstr "Ryan Lerch, ryanlerch na gmail bodka com"

#: tutorial-interpolate.xml:9(para)
msgid "This document explains how to use Inkscape's Interpolate extension"
msgstr ""
"Tento dokument vysvetľuje ako používať rozšírenie Inkscape Interpolácia."

#: tutorial-interpolate.xml:14(title)
msgid "Introduction"
msgstr "Úvod"

#: tutorial-interpolate.xml:15(para)
msgid ""
"Interpolate does a <firstterm>linear interpolation</firstterm> between two or "
"more selected paths. It basically means that it “fills in the gaps” between "
"the paths and transforms them according to the number of steps given."
msgstr ""
"Interpolácia vykonáva <firstterm>lineárnu interpoláciu</firstterm> medzi "
"dvomi alebo viacerými vybranými cestami. V podstate to znamená, že „vypĺňa "
"medzery“ medzi cestami a transformuje ich podľa počtu požadovaných krokov."

#: tutorial-interpolate.xml:16(para)
msgid ""
"To use the Interpolate extension, select the paths that you wish to "
"transform, and choose <command>Extensions &gt; Generate From Path &gt; "
"Interpolate</command> from the menu."
msgstr ""
"Rozšírenie Interpolácia použijete tak, že vyberiete dve cesty, ktoré chcete "
"transformovať a zvolíte z ponuky <command>Rozšírenia &gt; Vytvoriť z "
"cesty &gt; Interpolácia</command>."

#: tutorial-interpolate.xml:17(para)
msgid ""
"Before invoking the extension, the objects that you are going to transform "
"need to be <emphasis>paths</emphasis>. This is done by selecting the object "
"and using <command>Path &gt; Object to Path</command> or <keycap>Shift+Ctrl"
"+C</keycap>. If your objects are not paths, the extension will do nothing."
msgstr ""
"Pred spustením rozšírenia musia byť objekty, ktoré sa chystáte transformovať, "
"<emphasis>cesty</emphasis>. To docielite tak, že vyberiete objekt a použijete "
"<command>Cesta &gt; Objekt na cestu</command> alebo <keycap>Shift+Ctrl+C</"
"keycap>. Ak vaše objekty nie sú cesty, efekt neurobí nič."

#: tutorial-interpolate.xml:21(title)
msgid "Interpolation between two identical paths"
msgstr "Interpolácia medzi dvomi zhodnými cestami"

#: tutorial-interpolate.xml:22(para)
msgid ""
"The simplest use of the Interpolate extension is to interpolate between two "
"paths that are identical. When the extension is called, the result is that "
"the space between the two paths is filled with duplicates of the original "
"paths. The number of steps defines how many of these duplicates are placed."
msgstr ""
"Najjednoduchším použitím rozšírenia Interpolácia je interpolácia medzi dvomi "
"cestami, ktoré sú identické. Po zavolaní efektu je výsledkom vyplnenie "
"priestoru medzi dvomi cestami duplikátmi pôvodných ciest. Počet krokov "
"definuje koľko týchto duplikátov sa umiestni."

#: tutorial-interpolate.xml:23(para) tutorial-interpolate.xml:47(para)
msgid "For example, take the following two paths:"
msgstr "Vezmime si napríklad nasledovné dve cesty:"

#: tutorial-interpolate.xml:32(para)
msgid ""
"Now, select the two paths, and run the Interpolate extension with the "
"settings shown in the following image."
msgstr ""
"Teraz tieto dve cesty vyberte a spustite rozšírenie Interpolácia s "
"nastaveniami podľa nasledovného obrázka."

#: tutorial-interpolate.xml:41(para)
msgid ""
"As can be seen from the above result, the space between the two circle-shaped "
"paths has been filled with 6 (the number of interpolation steps) other circle-"
"shaped paths. Also note that the extension groups these shapes together."
msgstr ""
"Ako vidno z výsledku hore, priestor medzi dvomi cestami v tvare kružnice bol "
"vyplnený šiestimi (počet krokov interpolácie) ďalšími cestami v tvare "
"kružnice. Tiež si všimnite, že rozšírenie tieto útvary zoskupí."

#: tutorial-interpolate.xml:45(title)
msgid "Interpolation between two different paths"
msgstr "Interpolácia medzi dvomi odlišnými cestami"

#: tutorial-interpolate.xml:46(para)
msgid ""
"When interpolation is done on two different paths, the program interpolates "
"the shape of the path from one into the other. The result is that you get a "
"morphing sequence between the paths, with the regularity still defined by the "
"Interpolation Steps value."
msgstr ""
"Pri vykonaní Interpolácie na dvoch odlišných cestách program bude "
"interpolovať tvar cesty z jednej po druhú. Výsledkom je meniaca sa postupnosť "
"medzi cestami, pričom pravidelnosť definuje hodnota Krokov interpolácie."

#: tutorial-interpolate.xml:56(para)
msgid ""
"Now, select the two paths, and run the Interpolate extension. The result "
"should be like this:"
msgstr ""
"Teraz vyberte tieto dve cesty a spustite rozšírenie Interpolácia. Výsledok by "
"mal byť nasledovný:"

#: tutorial-interpolate.xml:65(para)
msgid ""
"As can be seen from the above result, the space between the circle-shaped "
"path and the triangle-shaped path has been filled with 6 paths that progress "
"in shape from one path to the other."
msgstr ""
"Ako vidno z výsledku hore, priestor medzi cestou v tvare kružnice a cestou v "
"tvare trojuholníka bol vyplnený šiestimi cestami v tvaroch postupujúcich od "
"kružnice po trojuholník."

#: tutorial-interpolate.xml:67(para)
msgid ""
"When using the Interpolate extension on two different paths, the "
"<emphasis>position</emphasis> of the starting node of each path is important. "
"To find the starting node of a path, select the path, then choose the Node "
"Tool so that the nodes appear and press <keycap>TAB</keycap>. The first node "
"that is selected is the starting node of that path."
msgstr ""
"Pri použití rozšírenia Interpolácia na dve odlišné cesty, na "
"<emphasis>polohe</emphasis> počiatočného uzla každej cesty záleží. Počiatočný "
"uzol cesty nájdete tak, že cestu vyberiete, potom vyberiete nástroj Uzol, aby "
"sa zobrazili uzly a stlačíte <keycap>Tab</keycap>. Prvý vybraný uzol je "
"počiatočný uzol danej cesty."

#: tutorial-interpolate.xml:69(para)
msgid ""
"See the image below, which is identical to the previous example, apart from "
"the node points being displayed. The node that is green on each path is the "
"starting node."
msgstr ""
"Pozrite si obrázok dolu, ktorý je rovnaký ako predchádzajúci príklad okrem "
"toho, že tu sú zobrazené body uzlov. Uzol vyznačený zelenou na každej ceste "
"je počiatočný uzol."

#: tutorial-interpolate.xml:78(para)
msgid ""
"The previous example (shown again below) was done with these nodes being the "
"starting node."
msgstr ""
"Predchádzajúci príklad (znova zobrazený dolu) bol vykonaný s týmito uzlami "
"ako počiatočnými."

#: tutorial-interpolate.xml:87(para)
msgid ""
"Now, notice the changes in the interpolation result when the triangle path is "
"mirrored so the starting node is in a different position:"
msgstr ""
"Teraz si všimnite zmeny vo výsledku interpolácie, keď je trojuholníková cesta "
"zrkadlovo otočená, takže počiatočný uzol je v inej polohe:"

#: tutorial-interpolate.xml:106(title)
msgid "Interpolation Method"
msgstr "Metóda interpolácie"

#: tutorial-interpolate.xml:107(para)
msgid ""
"One of the parameters of the Interpolate extension is the Interpolation "
"Method. There are 2 interpolation methods implemented, and they differ in the "
"way that they calculate the curves of the new shapes. The choices are either "
"Interpolation Method 1 or 2."
msgstr ""
"Jedným z parametrov rozšírenia Interpolácia je metóda interpolácie. Sú "
"implementované dve metódy interpolácie a líšia sa spôsobom, akým prebieha "
"výpočet kriviek nových tvarov. Možnosti sú buď metóda interpolácie 1 alebo 2."

#: tutorial-interpolate.xml:109(para)
msgid ""
"In the examples above, we used Interpolation Method 2, and the result was:"
msgstr "V príkladoch hore sme použili Metódu interpolácie 2 a výsledkom bolo:"

#: tutorial-interpolate.xml:118(para)
msgid "Now compare this to Interpolation Method 1:"
msgstr "Teraz ho porovnajte s výsledkom Metódy interpolácie 1:"

#: tutorial-interpolate.xml:127(para)
msgid ""
"The differences in how these methods calculate the numbers is beyond the "
"scope of this document, so simply try both, and use which ever one gives the "
"result closest to what you intend."
msgstr ""
"Rozdiel v spôsobe výpočtu čísiel medzi týmito metódami je nad rozsah tohto "
"dokumentu, preto jednoducho skúste obe a použite tú, ktorá dosiahne výsledok "
"blízky tomu, čo ste si predstavovali."

#: tutorial-interpolate.xml:132(title)
msgid "Exponent"
msgstr "Exponent"

#: tutorial-interpolate.xml:134(para)
msgid ""
"The <firstterm>exponent parameter</firstterm> controls the spacing between "
"steps of the interpolation. An exponent of 0 makes the spacing between the "
"copies all even."
msgstr ""
"<firstterm>Parameter exponent</firstterm> ovláda rozostupy medzi krokmi "
"interpolácie. Exponent 0 znamená, že rozostupy medzi jednotlivými kópiami sú "
"rovnomerné."

#: tutorial-interpolate.xml:136(para)
msgid "Here is the result of another basic example with an exponent of 0."
msgstr "Tu je výsledok ďalšieho jednoduchého príkladu s exponentom 0."

#: tutorial-interpolate.xml:145(para)
msgid "The same example with an exponent of 1:"
msgstr "Rovnaký príklad s exponentom 1:"

#: tutorial-interpolate.xml:154(para)
msgid "with an exponent of 2:"
msgstr "s exponentom 2:"

#: tutorial-interpolate.xml:163(para)
msgid "and with an exponent of -1:"
msgstr "a s exponentom -1:"

#: tutorial-interpolate.xml:172(para)
msgid ""
"When dealing with exponents in the Interpolate extension, the "
"<emphasis>order</emphasis> that you select the objects is important. In the "
"examples above, the star-shaped path on the left was selected first, and the "
"hexagon-shaped path on the right was selected second."
msgstr ""
"Pri práci s exponentami rozšírenia Interpolácia na <emphasis>poradí</"
"emphasis> v akom objekty vyberiete záleží. V hore uvedených príkladoch bol "
"najskôr vybraný útvar v tvare hviezdy vľavo a šesťuholníkový útvar vpravo bol "
"vybraný druhý v poradí."

#: tutorial-interpolate.xml:174(para)
msgid ""
"View the result when the path on the right was selected first. The exponent "
"in this example was set to 1:"
msgstr ""
"Pozrite si výsledok dosiahnutý vybraním najprv útvaru vpravo. Exponent v "
"tomto príklade bol nastavený na 1:"

#: tutorial-interpolate.xml:185(title)
msgid "Duplicate Endpaths"
msgstr "Duplikovať konce ciest"

#: tutorial-interpolate.xml:186(para)
msgid ""
"This parameter defines whether the group of paths that is generated by the "
"extension <emphasis>includes a copy</emphasis> of the original paths that "
"interpolate was applied on."
msgstr ""
"Tento parameter definuje, či skupina ciest vytvorených týmto rozšírením "
"<emphasis>obsahuje kópiu</emphasis> pôvodných ciest, na ktoré sa interpolácia "
"aplikovala."

#: tutorial-interpolate.xml:190(title)
msgid "Interpolate Style"
msgstr "Interpolovať štýl"

#: tutorial-interpolate.xml:191(para)
msgid ""
"This parameter is one of the neat functions of the interpolate extension. It "
"tells the extension to attempt to change the style of the paths at each step. "
"So if the start and end paths are different colors, the paths that are "
"generated will incrementally change as well."
msgstr ""
"Tento parameter je jedna z pekných funkcií rozšírenia Interpolácia. Hovorí "
"efektu, aby sa pokúsil zmeniť štýl ciest v každom kroku. Takže ak má "
"počiatočná a koncová cesta odlišnú farbu, vytvorené cesty sa budú tiež "
"postupne medzi nimi meniť."

#: tutorial-interpolate.xml:193(para)
msgid ""
"Here is an example where the Interpolate Style function is used on the fill "
"of a path:"
msgstr ""
"Tu je príklad, kde bola funkcia Interpolovať štýl použitá na výplň cesty:"

#: tutorial-interpolate.xml:203(para)
msgid "Interpolate Style also affects the stroke of a path:"
msgstr "Interpolovať štýl tiež berie do úvahy ťah cesty:"

#: tutorial-interpolate.xml:212(para)
msgid ""
"Of course, the path of the start point and the end point does not have to be "
"the same either:"
msgstr ""
"Samozrejme, ani cesta počiatočného bodu a koncového bodu nemusí byť rovnaká:"

#: tutorial-interpolate.xml:223(title)
msgid "Using Interpolate to fake irregular-shaped gradients"
msgstr ""
"Použitie Interpolácie na vytvorenie nepravých farebných prechodov s "
"nepravidelným tvarom"

#: tutorial-interpolate.xml:225(para)
#, fuzzy
msgid ""
"At the time when gradient meshes were not implemented in Inkscape, it was not "
"possible to create a gradient other than linear (straight line) or radial "
"(round). However, it could be faked using the Interpolate extension and "
"Interpolate Style. A simple example follows — draw two lines of different "
"strokes:"
msgstr ""
"V Inkscape (zatiaľ) nie je možné vytvoriť iný farebný prechod ako lineárny "
"(priama čiara) alebo radiálny (okrúhly). Je ich však možné napodobniť pomocou "
"rozšírenia Interpolácia a voľby Interpolovať štýl. Nasleduje jednoduchý "
"príklad — nakreslite dve čiary s odlišnými ťahmi:"

#: tutorial-interpolate.xml:233(para)
msgid "And interpolate between the two lines to create your gradient:"
msgstr ""
"A interpolujte medzi týmito dvomi čiarami, čím sa vytvorí farebný prechod:"

#: tutorial-interpolate.xml:245(title)
msgid "Conclusion"
msgstr "Záver"

#: tutorial-interpolate.xml:246(para)
msgid ""
"As demonstrated above, the Inkscape Interpolate extension is a powerful tool. "
"This tutorial covers the basics of this extension, but experimentation is the "
"key to exploring interpolation further."
msgstr ""
"Ak bolo predvedené vyššie, rozšírenia Inkscape Interpolácia je mocný nástroj. "
"Tento návod pokrýva základy použitia tohto efektu, ale experimentovanie je "
"kľúčom k ďalšiemu objavovaniu interpolácie."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-interpolate.xml:0(None)
msgid "translator-credits"
msgstr "Ivan Masár <helix84@centrum.sk>, 2010, 2015."

#, fuzzy
#~ msgid "@@image: 'interpolate-f04.svg'; md5=3c0108bed36757c6cce0d5a4ea4a1e36"
#~ msgstr "@@image: 'interpolate-f04.svg'; md5=395c8919488dfea2833e4d14784d0eca"

#~ msgid "@@image: 'interpolate-f02.svg'; md5=e3df335f4a300dca1507f00e7ed21253"
#~ msgstr "@@image: 'interpolate-f02.svg'; md5=e3df335f4a300dca1507f00e7ed21253"

#~ msgid "@@image: 'interpolate-f11.svg'; md5=d99206eb12e84db281b3d7c2b77103db"
#~ msgstr "@@image: 'interpolate-f11.svg'; md5=d99206eb12e84db281b3d7c2b77103db"

#~ msgid "Interpolation between two of the same path"
#~ msgstr "Interpolácia medzi dvomi rovnakými cestami"
