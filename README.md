Inkscape Documentation
======================

This includes all files required for creating localized documentation for Inkscape.

At the time of this writing this includes:
 - Inkscape man pages (*man*)
 - Inkscape keyboard and mouse reference (*keys*)
 - Inkscape tutorials (*tutorials*)

It also has rules to create translation statistics (*statistics*).


Creating documentation
----------------------

Each of the subdirectories has a README with detailed information on how to use (including software requirements).

Also you'll find a Makefile in each subdirectory for usage with GNU make (use `make help` for usage information)

If you're feeling lucky (or know that you have all requirements installed)
you can also use the convenience targets from the top level Makefile:
 - use `make` to generate all documentation
 - use `make help` to see additional usage information

*Note: As git does not track file modification times `make` might not be able to determine which targets need to be
 re-made after checking out new files. In order to re-make all files use `make --always-make` (or `make -B`).*